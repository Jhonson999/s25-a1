db.fruits.insertMany([
  { "name": "Banana", "supplier": "Farmer Fruits Inc.", "stocks": 30, "price":20, "onSale": true},
  { "name": "Mango", "supplier": "Mango Magic Inc.", "stocks": 50, "price":70, "onSale": true},
  { "name": "Dragon fruit", "supplier": "Farmer Fruits Inc.", "stocks": 10, "price":60, "onSale": true},
  { "name": "Grapes", "supplier": "Fruity Co.", "stocks": 30, "price":100, "onSale": true},
  { "name": "Apple", "supplier": "Apple Valley", "stocks": 0, "price":20, "onSale": false},
  { "name": "Papaya", "supplier": "Fruity Co.", "stocks": 15, "price":60, "onSale": true}

]);


 db.fruits.aggregate([
            {$match: {"onSale": true}},    
            {$count: "totalNumberofFruitsOnSale"}
        ])


 db.fruits.aggregate([
            {$match: {"stocks":{$gt: 20}}},    
            {$count: "totalNumberofFruitsWithStockMoreThan20"}
        ])

db.fruits.aggregate([
          {$match: {"onSale": true}},
          {$group: {_id: "$supplier",avg_price: {$avg: "$price"} }},
           
         ]);



db.fruits.aggregate([
          
          {$group: {_id: "$supplier",max_price: {$max: "$price"} }},
           
         ]);


db.fruits.aggregate([
          
          {$group: {_id: "$supplier",min_price: {$min: "$price"} }},
           
         ]);








