db.course_bookings.insertMany([
  { "courseId": "C001", "studentId": "S004", "isCompleted": true},
  { "courseId": "C002", "studentId": "S001", "isCompleted": false},
  { "courseId": "C001", "studentId": "S003", "isCompleted": true},
  { "courseId": "C003", "studentId": "S002", "isCompleted": false},
  { "courseId": "C001", "studentId": "S002", "isCompleted": true},
  { "courseId": "C004", "studentId": "S003", "isCompleted": false},
  { "courseId": "C002", "studentId": "S004", "isCompleted": true},
  { "courseId": "C003", "studentId": "S007", "isCompleted": false},
  { "courseId": "C001", "studentId": "S005", "isCompleted": true},
  { "courseId": "C004", "studentId": "S008", "isCompleted": false},
  { "courseId": "C001", "studentId": "S013", "isCompleted": true}
]);


//Aggregation in MongoDB
       // 		This is the act or prcoess of generating manipulated data and perform operations to create filtered
       // results that helps in analyzing data.
       // 		This helps in creating reports from analyzing the data provided in our documents.

//Aggregation Pipeline Syntax:

       db.collections.aggregate([
                {Stage 1}
                {Stage 2}
                {Stage 3}
       	]);

// Aggreagation Pipelines
       // Aggregations is done 2-3 steps typically. The first pipeline was with the use of $match.collections

       // $match is used to pass the documents or get the documents that will match our condition.collections

       //             Syntax: {$match:  {field: value}}

       // $group is used to group elements/documents together and create an analysis of these grouped documents.collections

       //              Syntax: {$group: {_id: <id>, fieldResult: "valueResult"}}

       // $sort can be used to change the order of the aggregated results
        
       //              Syntax: {$sort: {field: 0 or 1}}

       // $project - this will allow us to show or hide details.

       //               Syntax: {$project: {field: 0 or 1}}

       //$count - allow us to count the total number of items.
                         
       //                Syntax: {$count: "string"}

// Count all the documents in our collection.


       db.course_bookings.aggregate([
             {$group: {_id: null, count: {$sum: 1}}}
       	]);

       db.course_bookings.aggregate([
             {$match: {"isCompleted": true}},
             {$group: {_id: "$courseId", total: {$sum:1 }}}
       	]);


       db.course_bookings.aggregate([
            {$match: {"isCompleted": true}},
            {$project: {"courseId": 0}}
       	]);

       db.course_bookings.aggregate([
            {$match: {"isCompleted": true}},
            {$sort: {"courseId": -1}}
       	]);

     // Mini Activity:
        // a. Count the completed courses of student S013

        db.course_bookings.aggregate([
                {$match:{"studentId": "S013", "isCompleted": true}}
                {$group:{id: null, count: {$sum: 1 }} }

                ])
        db.course_bookings.aggregate([
            {$match: {studentId: "S013",isCompleted: true}},    
            {$count: "totalNumberofCompletedCourse"}
        ])

  

  db.orders.insertMany([
        {
                "cust_Id": "A123",
                "amount": 500,
                "status": "A"
        },
        {
                "cust_Id": "A123",
                "amount": 250,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "D"
        }
])


   db.orders.aggregate([
          {$match: {status: "A"}},
          {$group: {_id: "$cust_Id", total: {$sum: "$amount"} }}
   	]);	


           // $sum operator will total the values
           // $avg operator will average the results
           // $mas operator will show you the highest value
           // $min operator will show you the lowest value

// Get the avearge amount of the orders per cust_Id.

     db.orders.aggregate([
          {$match: {status: "A"}},
          {$group: {_id: "$cust_Id", total: {$avg: "$amount"} }}
   	]);

// Get the highest amount of the orders per cust_Id.
         db.orders.aggregate([
         {$group: {_id: "$cust_Id", total: {$max: "$amount"} }}
   	]);